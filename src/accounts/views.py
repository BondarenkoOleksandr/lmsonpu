import datetime

from django.contrib import messages
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, UpdateView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm, AccountLoginForm
from accounts.models import User


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User registered successfully')
        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'
    form_class = AccountLoginForm

    def form_valid(self, form):
        result = super().form_valid(form)
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'
    success_url = reverse_lazy('index')

    def get_redirect_url(self):
        return reverse('index')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.success(self.request, f'User {self.request.user} logged out')
        return super().dispatch(request, *args, **kwargs)


class AccountUpdateView(UpdateView):
    model = User
    template_name = 'profile.html'
    success_url = reverse_lazy('index')
    form_class = AccountUpdateForm

    def get_object(self, queryset=None):
        return self.request.user


class AccountPasswordChangeView(PasswordChangeView):
    template_name = 'password-change.html'
    success_url = reverse_lazy('index')
