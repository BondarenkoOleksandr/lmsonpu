from django.contrib.auth.forms import UserChangeForm, UserCreationForm, AuthenticationForm

from accounts.models import User


class AccountRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'register__input'
            })


class AccountLoginForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'register__input'
            })


class AccountUpdateForm(UserChangeForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = '__all__'
